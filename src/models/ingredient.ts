import {Injectable} from "@angular/core";

@Injectable()
export class IngredientModel {

  constructor(public name: string,
              public amount: number) {

  }

}
