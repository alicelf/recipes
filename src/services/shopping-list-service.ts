import {IngredientModel} from "../models/ingredient";

export class ShoppingListService {

  private ingredients: IngredientModel[] = []

  addItem(name: string, amount: number) {
    this.ingredients.push(new IngredientModel(name, amount))
  }

  addItems(items: IngredientModel[]) {
    this.ingredients.push(...items)
  }

  getItems() {
    return this.ingredients.slice()
  }

  removeItem(index: number) {
    this.ingredients.splice(index, 1)
  }

}
