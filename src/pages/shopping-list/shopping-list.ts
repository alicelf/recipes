import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";
import {IngredientModel} from "../../models/ingredient";
import {ShoppingListService} from "../../services/shopping-list-service";

@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage {

  listItems: IngredientModel[] = []

  constructor(private shoppingListService: ShoppingListService) {
  }

  onAddItem(form: NgForm) {
    this.shoppingListService.addItem(
      form.value.ingridientName,
      form.value.amount
    )
    form.reset()
    this.loadItems()
  }

  ionViewWillEnter() {
    this.loadItems()
  }

  onRemoveItem(index: number) {
    this.shoppingListService.removeItem(index)
    this.loadItems()
  }

  private loadItems() {
    this.listItems = this.shoppingListService.getItems()
  }

}
