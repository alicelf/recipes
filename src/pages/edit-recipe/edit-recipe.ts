import {Component, OnInit} from '@angular/core';
import {ActionSheetController, AlertController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {RecipesService} from "../../services/recipes";

@Component({
  selector: 'page-edit-recipe',
  templateUrl: 'edit-recipe.html',
})
export class EditRecipePage implements OnInit {

  mode = 'New'
  selectOptions = ['Easy', 'Medium', 'Hard']
  recipeForm: FormGroup;

  constructor(private toastCtrl: ToastController,
              private navCtrl: NavController,
              private actionSheetCtrl: ActionSheetController,
              private alertCtrl: AlertController,
              private navParams: NavParams,
              private recipesService: RecipesService) {
  }

  ngOnInit(): void {
    this.mode = this.navParams.get('mode')
    this.initializeForm()
  }

  private initializeForm() {
    this.recipeForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      difficulty: new FormControl('Medium', Validators.required),
      ingredients: new FormArray([])
    })
  }

  onManageIngredients() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'What do you want to do?',
      buttons: [
        {
          text: 'Add Ingredient',
          handler: () => {
            this.createNewIngredientAlert().present()
          }
        },
        {
          text: 'Remove All Ingredients',
          role: 'destructive',
          handler: () => {
            const fArray: FormArray = <FormArray>this.recipeForm.get('ingredients')
            const len = fArray.length
            if (len > 0) {
              for (let i = len - 1; i >= 0; i--) {
                fArray.removeAt(i)
              }
              const toast = this.toastCtrl.create({
                message: 'All Ingredients were deleted!',
                duration: 2000,
                position: 'bottom'
              })
              toast.present()
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        },
      ]
    })
    actionSheet.present()
  }

  private createNewIngredientAlert() {
    return this.alertCtrl.create({
      title: 'Add Ingredient',
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: data => {
            if (data.name.trim() == '' || data.name == null) {
              const toast = this.toastCtrl.create({
                message: 'Please, enter a valid value!',
                duration: 2000,
                position: 'bottom'
              })
              toast.present()
              return;
            }

            (<FormArray>this.recipeForm.get('ingredients'))
              .push(new FormControl(data.name, Validators.required))

            const toast = this.toastCtrl.create({
              message: 'Item added!',
              duration: 2000,
              position: 'bottom'
            })
            toast.present()
          }
        },
      ]
    })

  }

  onSubmit() {
    const val = this.recipeForm.value
    let ingredients = []

    if (val.ingredients.length > 0) {
      ingredients = val.ingredients.map(name => {
        return {
          name: name, amount: 1
        }
      })
    }

    this.recipesService.addRecipe(val.title, val.description, val.difficulty, ingredients)

    this.recipeForm.reset()
    this.navCtrl.popToRoot()
  }


}
